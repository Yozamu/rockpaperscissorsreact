export const CHOICES = ['Rock', 'Paper', 'Scissors'];
const WINS_OVER = [['Scissors'], ['Rock'], ['Paper']];
const LOSES_TO = [['Paper'], ['Scissors'], ['Rock']];

export const makeRandomChoice = () => {
    return CHOICES[Math.floor(Math.random() * CHOICES.length)];
};

export const duelResult = (firstChoice, secondChoice) => {
    const choiceIndex = CHOICES.indexOf(firstChoice);
    if (WINS_OVER[choiceIndex].includes(secondChoice)) {
        return 1;
    } else if (LOSES_TO[choiceIndex].includes(secondChoice)) {
        return -1;
    }
    return 0;
};