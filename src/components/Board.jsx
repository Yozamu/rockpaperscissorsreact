import React, { Fragment, useState, useEffect } from 'react';
import Player from './Player';
import BoardGameButtons from './BoardGameButtons';
import { duelResult } from '../choiceUtils';

const Board = () => {
    const [round, setRound] = useState(0);
    const [roundWinner, setRoundWinner] = useState(0);
    const [aiControlled, setAiControlled] = useState (false);
    const [roundPhase, setRoundPhase] = useState(0);
    const [scores, setScores] = useState({ player1: 0, player2: 0 });
    const [choices, setChoices] = useState({ player1: '', player2: '' });
    const [isFirstAiTurn, setIsFirstAiTurn] = useState(true);

    useEffect(() => {
        if (choices.player1 && choices.player2) {
            const result = duelResult(choices.player1, choices.player2);
            if (result === 1) {
                setScores({
                    ...scores,
                    player1: scores.player1 + 1
                });
                setRoundWinner(1);
            } else if (result === -1) {
                setScores({
                    ...scores,
                    player2: scores.player2 + 1
                });
                setRoundWinner(2);
            }
            setRoundPhase(0);
        }
    }, [choices]);

    const setChoice = (player, choice) => {
        setChoices({ 
            ...choices, 
            [player] : choice
        }, setIsFirstAiTurn(!isFirstAiTurn));
    };

    const resetGame = () => {
        setRound(0);
        setRoundWinner(0);
        setAiControlled(false);
        setRoundPhase(false);
        setScores({ player1: 0, player2: 0 });
        setChoices({ player1: '', player2: '' });
    };

    const displayRoundWinnerText = () => (
        <div>Player {roundWinner} won the last round</div>
    );

    const displayWaitingText = () => (
        <div>
            Next round is ready to be played <br />
            {aiControlled ? 'Computer' : 'Human'} vs Computer
        </div>
    );

    return (
        <Fragment>
            <div className="board-header">
                {round 
                    ? `Round ${round}`
                    : 'First round has not started yet'
                }
            </div>
            <div className='board'>
                <Player 
                    aiShouldPlay={isFirstAiTurn}
                    canPlay={roundPhase === 1}
                    isHuman={!aiControlled}
                    playerNumber={1}
                    lastChoice={choices.player1}
                    lastWinner={roundWinner}
                    score={scores.player1}
                    setChoice={setChoice}
                />
                {roundWinner
                    ? displayRoundWinnerText()
                    : displayWaitingText()
                }
                <Player 
                    aiShouldPlay={!isFirstAiTurn}
                    canPlay={roundPhase === 1}
                    playerNumber={2}
                    lastChoice={choices.player2}
                    lastWinner={roundWinner}
                    score={scores.player2}
                    setChoice={setChoice}
                />
            </div>
            <BoardGameButtons
                resetGame={resetGame}
                round={round}
                setRound={setRound}
                setRoundWinner={setRoundWinner}
                setAiControlled={setAiControlled}
                roundPhase={roundPhase}
                setRoundPhase={setRoundPhase}
                setChoices={setChoices}
            />
        </Fragment>
    );
};

export default Board;