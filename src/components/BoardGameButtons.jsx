import React, { useState } from 'react';

const BoardGameButtons = ({resetGame, round, roundPhase, setRound, setRoundWinner, setAiControlled, setRoundPhase, setChoices}) => {

    const startRound = () => {
        setChoices({ player1: '', player2: '' });
        setRound(round + 1);
        setRoundWinner(0);
        setRoundPhase(1);
    };

    return (
        <div>
            {roundPhase !== 1 && <button onClick={() => startRound()}>Start next round</button>}
            {roundPhase !== 1 && round === 0 && [
                <button onClick={() => setAiControlled(false)}>Human vs Computer</button>,
                <button onClick={() => setAiControlled(true)}>Computer vs Computer</button>
            ]}
            <button onClick={() => resetGame()}>Reset game</button>
        </div>
    );
};

export default BoardGameButtons;