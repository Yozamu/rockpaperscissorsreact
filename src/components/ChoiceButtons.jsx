import React, { useState } from 'react';
import { CHOICES } from '../choiceUtils';

const ChoiceButtons = ({play}) => {

    return (
        <div>
            {CHOICES.map(elem => (
                <button onClick={() => play(elem)}>{elem}</button>
            ))}
        </div>
    );
};

export default ChoiceButtons;