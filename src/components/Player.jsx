import React, { useState, useEffect } from 'react';
import ChoiceButtons from './ChoiceButtons';
import { makeRandomChoice } from '../choiceUtils';

const Player = ({aiShouldPlay, canPlay, isHuman, playerNumber, lastChoice, lastWinner, score, setChoice}) => {
    const [name, setName] = useState('Player');

    useEffect(() => {
        if (!isHuman && canPlay && aiShouldPlay && !lastChoice) {
            return play(makeRandomChoice());
        }
    }, [canPlay, aiShouldPlay]);

    const determineClassName = () => {
        if (playerNumber === lastWinner) {
            return 'player-winning';
        } else if (lastWinner !== 0) {
            return 'player-losing';
        }
        return 'player-neutral';
    };

    const play = (choice) => {
        setChoice(`player${playerNumber}`, choice);
    };

    return (
        <div className={determineClassName()}>
            <img src="logo192.png" alt="Player logo" /><br />
            {name} {playerNumber} : {score} points <br/>
            {!canPlay && lastChoice && `Last choice was ${lastChoice}`}
            {canPlay && isHuman && <ChoiceButtons play={play} />}
        </div>
    );
};

export default Player;